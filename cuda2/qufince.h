#pragma once
#include "../classic/bpattern.h"
#include <vector>

namespace apg {

void run_collision_kernel(const bpattern& and_mask, const bpattern& target, const std::vector<bpattern> &a, const std::vector<bpattern> &b, int gens1, int gens2);

}
