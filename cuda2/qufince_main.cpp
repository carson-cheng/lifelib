#include "../pattern2.h"
#include "qufince.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
std::vector<std::string> split(std::string s, std::string delimiter) {
    // source: https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::vector<std::string> res;

    while ((pos_end = s.find(delimiter, pos_start)) != std::string::npos) {
        token = s.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back (token);
    }

    res.push_back (s.substr (pos_start));
    return res;
}
int main(int argc, char *argv[]) {
    if (argc < 2){
        std::cout << "Please specify the input file." << std::endl;
        return 1;
    }
    std::cout << "Reading file..." << std::endl;
    std::string line;
    std::string file;
    std::ifstream inputFile(argv[1]);
    while (std::getline(inputFile, line)){
        file = file + line;
    }
    std::cout << "Done!" << std::endl;
    // resemble the catforce input files as much as possible
    std::string input_rle = split(split(file, "pat ")[1], " ")[0];
    std::string output_rle = split(split(file, "filter ")[1], " ")[1];
    std::cout << input_rle << std::endl;
    std::cout << output_rle << std::endl;
    if (input_rle.length() == 0 || output_rle.length() == 0){
        std::cout << "Invalid input file!" << std::endl;
        return 1;
    }
    apg::lifetree<uint32_t, 1> lt(1000);
    
    apg::pattern nglider(&lt, "16$18bo$19bo$17b3o!", "b3s23");
    apg::pattern sglider(&lt, "39$17b3o$19bo$18bo!", "b3s23");

    /*
    apg::pattern starting_still_life(&lt, "19$41b2o$42bo$35bo3b3o$34bobobo7b2o$34bobobob2o3bobo$33b2obobobo5bo$34bobobobo2b3o$34bobobobobo$33b2obobobobob2o3bo$34bobobobobobobobobo$34bobobobobobobob2o$33b2obobobobobobo$34bobobobobobobo$34bobobobobobob2o$33b2obobobobobobo$36bobobobobobo$33b2obobobobobobob2o$34bobobobobobobobobo$34bobobobobob2o3bo$33b2obobobobo$34bobobobo2b3o$34bobobobo5bo$33b2obobob2o3bobo$34bobobo7b2o$34bobo2b3o$33b2obo5bo$34bob2o3b2o$34bo$33b2o!", "b3s23");

    apg::pattern and_mask(&lt, "19$41b2o$42bo$35bo3b3o$34bobobo7b2o$34bobobob2o3bobo$33b2obobobo5bo$34bobobobo2b3o$34bobobobobo$33b2obobobobob2o3bo$34bobobobobobobobobo$34bobobobobobobob2o$33b2obobobobobobo$33b2obobobobobobo$33b2obobobobobob2o$36bobobobobobo$36bobobobobobo$33b2obobobobobobob2o$34bobobobobobobobobo$34bobobobobob2o3bo$33b2obobobobo$34bobobobo2b3o$34bobobobo5bo$33b2obobob2o3bobo$34bobobo7b2o$34bobo2b3o$33b2obo5bo$34bob2o3b2o$34bo$33b2o!", "b3s23");

    apg::pattern match_cells(&lt, "19$41b2o$42bo$35bo3b3o$34bobobo7b2o$34bobobob2o3bobo$33b2obobobo5bo$34bobobobo2b3o$34bobobobobo$33b2obobobobob2o3bo$34bobobobobobobobobo$34bobobobobobobob2o$33b2obobobobobobo$36bobobobobobo$33b2obobobobobob2o$36bobobobobobo$36bobobobobobo$33b2obobobobobobob2o$34bobobobobobobobobo$34bobobobobob2o3bo$33b2obobobobo$34bobobobo2b3o$34bobobobo5bo$33b2obobob2o3bobo$34bobobo7b2o$34bobo2b3o$33b2obo5bo$34bob2o3b2o$34bo$33b2o!", "b3s23");
    */

    apg::pattern starting_still_life(&lt, input_rle, "b3s23");

    apg::pattern and_mask(&lt, output_rle, "b3s23");

    apg::pattern match_cells = and_mask;

    std::vector<apg::pattern> g1a;
    std::vector<apg::pattern> g2a;
    std::vector<apg::pattern> g2b;

    for (int y = -7; y <= 7; y++) {
        for (int x = -7; x <= 7; x++) {
            for (int z = 0; z < 4; z++) {
                g1a.push_back(nglider[z](x, y));
                g2a.push_back(sglider[z](x, y));
            }
        }
        g2b.push_back(sglider(0, y));
    }

    std::cout << "Preparing..." << std::endl;

    std::vector<apg::bpattern> avec;

    for (uint32_t i = 0; i < g1a.size(); i++) {
        for (uint32_t j = 0; j < i; j++) {
            auto a = g1a[i];
            auto b = g1a[j];
            auto c = a + b;
            if ((c.totalPopulation() == 10) && (c[4] == a[4] + b[4])) {
                avec.push_back(c.flatlayer(0).to_bpattern());
            }
        }
    }

    std::cout << avec.size() << std::endl;

    std::vector<apg::bpattern> bvec;

    for (auto a : g2a) {
        for (auto b : g2b) {
            auto c = a + b;
            if ((c.totalPopulation() == 10) && (c[4] == a[4] + b[4])) {
                bvec.push_back((c + starting_still_life).flatlayer(0).to_bpattern());
            }
        }
    }

    std::cout << bvec.size() << std::endl;

    auto and_mask_b = and_mask.flatlayer(0).to_bpattern();
    auto target_b = match_cells.flatlayer(0).to_bpattern();

    run_collision_kernel(and_mask_b, target_b, bvec, avec, 200, 20);

    return 0;

}
